from selenium import webdriver
from math import log, sin

browser = webdriver.Chrome()
# Открыть страницу
browser.get("http://suninjuly.github.io/math.html")

# Найти на ней элемент
x = browser.find_element_by_css_selector('[id = "input_value"]').text

# Посчитать математическую функцию от x, Ввести ответ в текстовое поле.
browser.find_element_by_css_selector('[id = "answer"]').send_keys(str(log(abs(12 * sin(int(x))))))

# Отметить checkbox "Подтверждаю, что являюсь роботом". Выбрать radiobutton "Роботы рулят!". Нажать на кнопку Отправить.
for selector in ['[for="robotCheckbox"]', '[for="robotsRule"]', '.btn']:
    browser.find_element_by_css_selector(selector).click()
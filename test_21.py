import pytest
from selenium.webdriver.support.ui import WebDriverWait
from selenium import webdriver
import time
import math
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

result = ''


@pytest.fixture()
def browser():
    print("\nstart browser for test..")
    browser = webdriver.Chrome()
    yield browser
    print("\nquit browser..")
    browser.quit()


@pytest.mark.parametrize('num', ["https://stepik.org/lesson/236895/step/1",
                                 "https://stepik.org/lesson/236896/step/1",
                                 "https://stepik.org/lesson/236897/step/1",
                                 "https://stepik.org/lesson/236898/step/1",
                                 "https://stepik.org/lesson/236899/step/1",
                                 "https://stepik.org/lesson/236903/step/1",
                                 "https://stepik.org/lesson/236904/step/1",
                                 "https://stepik.org/lesson/236905/step/1"])
def test_guest_should_see_login_link(browser, num):
    link = f"{num}"
    browser.implicitly_wait(7)
    browser.get(link)
    textarea = browser.find_element_by_css_selector('.textarea')
    textarea.send_keys(str(math.log(int(time.time()))))
    button = WebDriverWait(browser, 5).until(EC.element_to_be_clickable((By.CLASS_NAME, "submit-submission")))
    button.click()
    msg = WebDriverWait(browser, 5).until(EC.visibility_of_element_located((By.CSS_SELECTOR, ".smart-hints__hint"))).text
    if msg != 'Correct!':
        global result
        result += msg
        with open('result.txt', 'w') as f:
            f.write(result)
    assert msg == 'Correct!'













from selenium import webdriver
import os

browser = webdriver.Chrome()
# Открыть страницу
browser.get("http://suninjuly.github.io/file_input.html")

# Заполнить текстовые поля
browser.find_element_by_name("firstname").send_keys('Nataliia')
browser.find_element_by_name("lastname").send_keys('Ananikova')
browser.find_element_by_name("email").send_keys('nata.ananikova@gmail.com')

# найти и прикрепить файл
current_dir = os.path.abspath(os.path.dirname(__file__))
file_path = os.path.join(current_dir, 'file.txt')
browser.find_element_by_name("file").send_keys(file_path)

# Нажать кнопку
submit_button = browser.find_element_by_class_name('btn.btn-primary').click()

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from math import log, sin

browser = webdriver.Chrome()
browser.get("http://suninjuly.github.io/explicit_wait2.html")

# говорим Selenium проверять в течение 12 секунд, цена не опустится до 100
WebDriverWait(browser, 12).until(EC.text_to_be_present_in_element((By.ID, "price"), '100'))

# Нажать кнопку
submit_button = browser.find_element_by_class_name('btn.btn-primary').click()

# Найти элемент внизу страницы
x = browser.find_element_by_css_selector('[id = "input_value"]').text

# Посчитать математическую функцию от x, Ввести ответ в текстовое поле.
browser.find_element_by_css_selector('[id = "answer"]').send_keys(str(log(abs(12 * sin(int(x))))))

browser.find_element_by_css_selector('[id = "solve"]').click()

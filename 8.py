from selenium import webdriver
from math import log, sin

browser = webdriver.Chrome()

# Открыть страницу
browser.get('http://suninjuly.github.io/redirect_accept.html')

# Найти на ней кнопку и нажать
browser.find_element_by_class_name('btn.btn-primary').click()

# Работа с несколькими окнами
new_window = browser.window_handles[1]
browser.switch_to.window(new_window)

# Найти элемент на следующей странице
x = browser.find_element_by_css_selector('[id = "input_value"]').text

# Посчитать математическую функцию от x, Ввести ответ в текстовое поле.
browser.find_element_by_css_selector('[id = "answer"]').send_keys(str(log(abs(12 * sin(int(x))))))

browser.find_element_by_class_name('btn.btn-primary').click()

from selenium import webdriver
from math import log, sin

browser = webdriver.Chrome()

# Открыть страницу
browser.get('http://suninjuly.github.io/alert_accept.html')

# Найти на ней кнопку и нажать
browser.find_element_by_class_name('btn.btn-primary').click()

# Другой вариант модального окна, который предлагает пользователю выбор согласиться с
# сообщением или отказаться от него, называется confirm.
# Для переключения на окно confirm используется та же команда, что и в случае с alert:
confirm = browser.switch_to.alert
confirm.accept()

# Найти элемент на следующей странице
x = browser.find_element_by_css_selector('[id = "input_value"]').text

# Посчитать математическую функцию от x, Ввести ответ в текстовое поле.
browser.find_element_by_css_selector('[id = "answer"]').send_keys(str(log(abs(12 * sin(int(x))))))

browser.find_element_by_class_name('btn.btn-primary').click()

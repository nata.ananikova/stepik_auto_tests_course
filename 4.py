from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select


browser = webdriver.Chrome()

# Открыть страницу
browser.get('http://suninjuly.github.io/selects1.html')

# Найти на ней элементы
num1 = browser.find_element_by_css_selector('[id = "num1"]').text
num2 = browser.find_element_by_css_selector('[id = "num2"]').text

select = Select(browser.find_element(By.TAG_NAME, "select"))
select.select_by_value(f'{int(num1) + int(num2)}')

submit_button = browser.find_element_by_class_name('btn.btn-default').click()
